var taskModel = require('./../models/task');
var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  taskModel.find({}, function(err, users) {
	if (err) throw err;

	  // object of all the users
	  return res.json(users);
	});
});

router.post('/add', function(req, res, next) {
	console.log(req.body, "body")
	// create a new user
	var newUser = taskModel({
	  name: req.body.name,
	  id: req.body.id
	});

	// save the user
	newUser.save(function(err, data) {
	  if (err) throw err;
		console.log(data);
	  return res.json(data);
	});
});


router.put('/update', function(req, res, next) {
	// get a user with ID of 1
	console.log(req.query, req.params, req.body)
	taskModel.findOneAndUpdate({id: req.query.id}, {name: req.body.name}, function(err, user) {
	  if (err) throw err;

	  return res.json("Data updated");
	});

});

router.delete('/delete', function(req, res, next) {
	taskModel.findOneAndRemove({id: req.query.id}, function(err) {
		if (err) throw err;
	
		return res.json("Data deleted");
	});
});

module.exports = router;