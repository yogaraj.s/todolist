import React, { Component } from 'react';
import logo from './logo.svg';
import axios from 'axios';
import './App.css';



const TodoListForm = ({saveTodo, name, id, changeMethod}) => {
  let input;
  let obj = {
    name: name,
    id: id
  }
    return (
      <div>
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>To do List</h2>
        </div>
        <div className="App-intro">
          <input type="text" name="text" placeholder="Enter your task" value={name} id={id} onChange={changeMethod}/>
          <button onClick={() => { saveTodo(obj); }}>Save</button>
        </div>
      </div>
    );
}

const TableContent = ({listValues, edit, deleteTask}) => {
  let mapped;
  if(listValues.length === 0) {
    return (
      <div>
      </div>
    )
  } else {
    mapped = listValues.map((value) => {
        return (
          <tr>
            <td>{value.id}</td>
            <td>{value.name}</td>
            <td key={value.id}>
              <button onClick={() => {edit(value)}}>Edit</button>
              <button onClick={() => {deleteTask(value)}}>Delete</button>
            </td>
          </tr>
        )
    });
  }

  return ([mapped])
}

class App extends Component {
  
  constructor(props) {
    super(props);
    this.toDoList = [];
    this.state = {
      toDoList: []
    }
    this.getList = 'http://localhost:4000/tasks'

  }

  getInitialState = () => { 
    return {
      value: this.props.name,
      id: 0
    };
  }

  // Lifecycle method
  componentDidMount(){
    // Make HTTP reques with fetch
    axios.get(this.getList)
      .then((res) => {
        this.setState({toDoList: res.data});
      }).catch((err) => {
        console.log(err);
      })
  }


  saveTodo = (value) => {
    if((value.id == "") || (value.id == undefined)) {
      let tempObj = {};
      tempObj.name = value.name;
      if(this.state.toDoList.length === 0){
        tempObj.id = 1;
      } else {  
        tempObj.id = this.state.toDoList[this.state.toDoList.length - 1].id + 1;
      }
      axios.post('http://localhost:4000/tasks/add', tempObj).then((res) => {
         this.state.toDoList.push(tempObj);
         this.setState({toDoList: this.state.toDoList});
         this.setState({value: '', id:''});
      });
    } else {
      axios.put('http://localhost:4000/tasks/update?id='+value.id, {"name": value.name}).then((res) => {
        let updatedValueIndex = this.state.toDoList.map(function(e) { return e.id; }).indexOf(value.id);
        this.state.toDoList[updatedValueIndex].name = value.name;
        this.setState({toDoList: this.state.toDoList});
        this.setState({value: '', id:''});
      });
    }
  };

  updateTodo = (value, props) => {
    this.setState({value: value.name, id: value.id});
  }

  deleteTodo = (value) => {
    
    axios.delete("http://localhost:4000/tasks/delete?id="+value.id)
    .then((res)=> {
      let posToDel = this.state.toDoList.indexOf(value);
      this.state.toDoList.splice(posToDel, 1);
      this.setState({toDoList: this.state.toDoList});
    });
  }

  handleChange = (event) => {
    this.setState({value: event.target.value});
  }


  render() {

    return (
      <div className="App">
        <TodoListForm saveTodo={this.saveTodo.bind(this)} name={this.state.value} id={this.state.id} changeMethod={this.handleChange}/>
        <div className="container tableStyle">
          <table className="table table-bordered">
            <thead>
              <tr>
                <th width="10%">Task Id</th>
                <th width="70%">To Do Task</th>
                <th width="20%">Actions</th>
              </tr>
            </thead>
            <tbody className="tbodyStyle">
              <TableContent listValues={this.state.toDoList} edit={this.updateTodo.bind(this)} deleteTask={this.deleteTodo.bind(this)}/> 
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default App;